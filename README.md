# README #

This project was for a CSE 219 (Java II) homework.

The goal was to create a program that would allow a user to create a mocked course and add assignments to a class calendar.  An HTML page is then generated for the course and can be viewed in a browser.