package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.LectureDialog;
import csb.gui.YesNoCancelDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;


public class LectureEditController {
    LectureDialog ld;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ld = new LectureDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        
    }
    public void handleAddLectureRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ld.showAddLectureDialog();
        
        if (ld.wasCompleteSelected()) {
            Lecture lect = ld.getLecture();
            
            course.addLecture(lect);
        }
        else {
            // USER PRESSED CANCEL, DO NOTHING
        }
    }
    
    public void handleEditLectureRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ld.showEditLectureDialog(itemToEdit);
        
        if (ld.wasCompleteSelected()) {
            Lecture lect = ld.getLecture();
            itemToEdit.setTopic(lect.getTopic());
            itemToEdit.setSessions(lect.getSessions());
        }
        else {
            // USER HIT CANCEL, DO NOTHING
        }
    }
    
    public void handleRemoveLectureRequest(CSB_GUI gui, Lecture itemToRemove) {
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        String selection = yesNoCancelDialog.getSelection();
        
        if (selection.equals(YesNoCancelDialog.YES)) {
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    
    public int handleMoveLectureUpRequest(CSB_GUI gui, Lecture itemToMove){
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        
        int oldIndex = course.getLectures().indexOf(itemToMove);
        
        if (oldIndex != 0)
            course.swapLectures(oldIndex, oldIndex-1);
        
        return oldIndex - 1;
    }
    
    public int handleMoveLectureDownRequest(CSB_GUI gui, Lecture itemToMove){
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        
        int oldIndex = course.getLectures().indexOf(itemToMove);
        
        if (oldIndex != course.getLectures().size()-1)
            course.swapLectures(oldIndex, oldIndex + 1);
        
        return oldIndex + 1;
    }
    

//    public void handleAddLectureRequest(CSB_GUI aThis) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
