package csb.controller;

import csb.data.Course;
import csb.data.CourseDataManager;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.ProgressDialog;
import javafx.stage.Stage;
/**
 *
 * @author PaulSack
 */
public class ProgressController {
    ProgressDialog pd;
    MessageDialog messageDialog;
    
    
    public ProgressController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog) {
        pd = new ProgressDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;   
    }
    
    public void handleStartProgress(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        pd.beginProgress(course);
    }
}
