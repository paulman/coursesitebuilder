package csb.gui;

import csb.data.Course;
import csb.data.Lecture;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.ComboBox;

public class LectureDialog extends Stage {

    //THIS IS THE OBJECT DATA BEHIND THIS UI

    Lecture lecture;

    //GUI CONTROLS
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicLabel;
    TextField topicTextField;
    Label sessionLabel;
    ComboBox sessionComboBox;
    Button completeButton;
    Button cancelButton;

    // KEEP TRACK OF WHICH BUTTON WAS PRESSED
    String selection;
    
    // COMBO BOX ARRAY
    ObservableList<String> sessions = FXCollections.observableArrayList("1","2","3","4","5","6","7","8","9","10");

    // CONSTANTS FOR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TOPIC_PROMPT = "Topic: ";
    public static final String SESSION_PROMPT = "Sessions: ";
    public static final String LECTURE_HEADING = "Lecture Details";
    public static final String ADD_LECTURE_TITLE = "Add New Lecture";
    public static final String EDIT_LECTURE_TITLE = "Edit Lecture";

    public LectureDialog(Stage primaryStage, Course course, MessageDialog messageDialog) {

        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        headingLabel = new Label(LECTURE_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);

        topicLabel = new Label(TOPIC_PROMPT);
        topicLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicTextField = new TextField();
        topicTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lecture.setTopic(newValue);
        });

        sessionLabel = new Label(SESSION_PROMPT);
        sessionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        sessionComboBox = new ComboBox(sessions);
        sessionComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            lecture.setSessions(Integer.parseInt(newValue.toString()));
        });

        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);

        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button) ae.getSource();
            LectureDialog.this.selection = sourceButton.getText();
            LectureDialog.this.hide();
        };

        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(topicLabel, 0, 1, 1, 1);
        gridPane.add(topicTextField, 1, 1, 1, 1);
        gridPane.add(sessionLabel, 0, 2, 1, 1);
        gridPane.add(sessionComboBox, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);

        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }

    public String getSelection() {
        return selection;
    }

    public Lecture getLecture() {
        return lecture;
    }

    public Lecture showAddLectureDialog() {
        setTitle(ADD_LECTURE_TITLE);

        lecture = new Lecture();

        topicTextField.setText(lecture.getTopic());
        sessionComboBox.setValue(lecture.getSessions());

        this.showAndWait();

        return lecture;
    }

    public void loadGUIData() {
        topicTextField.setText(lecture.getTopic());
        sessionComboBox.setValue(lecture.getSessions());
    }

    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }

    public void showEditLectureDialog(Lecture lectureToEdit) {
        setTitle(EDIT_LECTURE_TITLE);

        lecture = new Lecture();
        lecture.setTopic(lectureToEdit.getTopic());
        lecture.setSessions(lectureToEdit.getSessions());

        loadGUIData();
        this.showAndWait();
    }


}
