/**
 * Paul Mannarino paul.mannarino@stonybrook.edu 108060069
 *
 * Much of this code was borrowed from Mckenna's BetterProgressBar.java code but
 * there have been changes.
 */
package csb.gui;

import static csb.CSB_StartupConstants.*;
import csb.data.Course;
import csb.data.CoursePage;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import csb.gui.MessageDialog;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import javafx.geometry.Insets;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

/**
 *
 * @author McKillaGorilla / Paul Sack
 */
public class ProgressDialog extends Stage {

    String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    String CLASS_INDICATOR_STYLE = "progress_pie";
    String CLASS_BAR_STYLE = "progress_bar";

    ProgressBar bar;
    ProgressIndicator indicator;
    Button button;
    Label processLabel;
    Label siteToExportLabel;
    ReentrantLock progressLock;
    int[] values;
    int numTasks = 0;

    public ProgressDialog(Stage primaryStage, Course course, MessageDialog messageDialog) {

        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        bar = new ProgressBar(-1);
        indicator = new ProgressIndicator(-1);
        indicator.getStyleClass().add(CLASS_INDICATOR_STYLE);
        indicator.setStyle("-fx-progress-color:#ff33cc;");
        bar.getStyleClass().add(CLASS_BAR_STYLE);

        processLabel = new Label();
        processLabel.setFont(new Font("Serif", 30));

        gridPane.add(bar, 1, 3, 3, 1);
        gridPane.add(indicator, 2, 3, 1, 1);
        gridPane.add(processLabel, 0, 2, 5, 1);
        gridPane.setPrefSize(500, 200);
        gridPane.getColumnConstraints().add(new ColumnConstraints(100));
        gridPane.getColumnConstraints().add(new ColumnConstraints(100));

        Scene scene = new Scene(gridPane);
        this.setScene(scene);
    }

    public void beginProgress(Course course) {
        List<CoursePage> pages = course.getPages();
        progressLock = new ReentrantLock();
        Task<Void> task = new Task<Void>() {
            double perc;
            int pageIndex = 0;

            @Override
            protected Void call() throws Exception {
                try {
                    progressLock.lock();
                    for (pageIndex = 1; pageIndex <= pages.size(); pageIndex++) {
                        if (pageIndex == 1) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                        // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                perc = (double) pageIndex / pages.size();
                                bar.setProgress(perc);
                                indicator.setProgress(perc);
                                processLabel.setText("Exporting " + pages.get(pageIndex - 1).toString() + " Completed");
                            }
                        });
                        // SLEEP EACH FRAME
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                } finally {
                    progressLock.unlock();
                }
                return null;
            }
        };
        // THIS GETS THE THREAD ROLLING
        Thread thread = new Thread(task);
        thread.start();
        this.showAndWait();
    }
}
