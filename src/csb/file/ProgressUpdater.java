package csb.file;

import csb.data.Course;
import csb.data.CoursePage;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import csb.file.CourseSiteExporter;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.scene.layout.GridPane;

/**
 *
 * @author McKillaGorilla
 */
public class ProgressUpdater extends Stage {

    ProgressBar bar;
    ProgressIndicator indicator;
    Button button;
    Label processLabel;
    ReentrantLock progressLock;
    int numTasks = 0;

    public void begin(MessageDialog messageDialog, CourseSiteExporter exporter, Course courseToExport) throws Exception {
        progressLock = new ReentrantLock();
        GridPane gridPane = new GridPane();
        
        List<CoursePage> pages = courseToExport.getPages();

        
        //VBox box = new VBox();

        //HBox toolbar = new HBox();
        bar = new ProgressBar(0);
        indicator = new ProgressIndicator(0);
        indicator.setStyle("font-size: 36pt");
        //toolbar.getChildren().add(bar);
        //toolbar.getChildren().add(indicator);
        
        

        processLabel = new Label();
        
        processLabel.setFont(new Font("Serif", 36));
        
        //box.getChildren().add(toolbar);
        
        
        //box.getChildren().add(processLabel);
        gridPane.add(bar, 6,2,1,1);
        gridPane.add(indicator,8, 2, 1, 1);
        gridPane.add(processLabel, 2, 3, 5, 2);
        
        //Scene scene = new Scene(gui, 200, 400);
        Scene scene = new Scene(gridPane);
        //scene.set
        
        messageDialog.setScene(scene);
        

        Task<Void> task = new Task<Void>() {
            double perc;
            int pageIndex = 0;

            @Override
            protected Void call() throws Exception {
                try {
                    progressLock.lock();
                    System.out.println("in task/call");
                    for (pageIndex = 1; pageIndex <= pages.size(); pageIndex++) {
                        System.out.println(pageIndex);

                        // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                System.out.println("in run");
                                perc = (double) pageIndex / pages.size();
                                bar.setProgress(perc);
                                indicator.setProgress(perc);
                                processLabel.setText("Exporting " + pages.get(pageIndex - 1).toString());

                            }
                        });

                        // SLEEP EACH FRAME
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    System.out.println("out of for loop");
                } finally {
                    progressLock.unlock();
                }
                return null;
            }
        };
        // THIS GETS THE THREAD ROLLING
        Thread thread = new Thread(task);
        thread.start();

        messageDialog.showAndWait();
    }
}
